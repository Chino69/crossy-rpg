
var nada = 2
var lives = 82;
var points = 0
// create a new scene named "Game"
let gameScene = new Phaser.Scene('Game');
// some parameters for our scene
gameScene.init = function() {
this.playerSpeed = 1.5;
this.enemySpeed = 2;
this.enemyMaxY = 280;
this.enemyMinY = 80;
}
gameScene.inutil = function (){
  nada = 0
}
gameScene.liveLoss = function(){
 lives --;

gameScene.liveGain = function(){
 lives = lives + 20;
}
gameScene.pointsEarn = function(){
 points ++;
}
gameScene.pointsLose = function(){
 points --;
}
}
// load asset files for our game
gameScene.preload = function() {
// load images
this.load.image('background', 'assets/background.png');
this.load.image('player', 'assets/player.png');
this.load.image('dragon', 'assets/dragon.png');
this.load.image('treasure', 'assets/treasure.png');
this.load.image('coin', 'assets/coin.png');
};
// executed once, after assets were loaded
gameScene.create = function() {
// background
let bg = this.add.sprite(0, 0, 'background');
 timeText = this.add.text(480, 100)
// change origin to the top-left of the sprite
livesText = this.add.text(40, 40)
bg.setOrigin(0, 0);
pointsText = this.add.text(40, 60)
// player
this.player = this.add.sprite(40, 180, 'player');
// scale down
this.player.setScale(0.5);
// goal
this.treasure = this.add.sprite(640 - 80, 180, 'treasure');
this.treasure.setScale(0.6);
// group of enemies
this.enemies = this.add.group({
  key: 'dragon',
  repeat: 1,
  setXY: {
    x: 110,
    y: 100,
    stepX: 80,
    stepY: 20
  }
});
// scale enemies
Phaser.Actions.ScaleXY(this.enemies.getChildren(), -0.5, -0.5);
// set speeds
Phaser.Actions.Call(this.enemies.getChildren(), function(enemy) {
  enemy.speed = Math.random() * 2 + 1;
}, this);
// player is alive
this.isPlayerAlive = true;
// reset camera
this.cameras.main.resetFX();
};
// executed on every frame (60 times per second)
gameScene.update = function() {
 livesText.setText('Lives: ' + lives.toString())
 timeText.setText('Time:' + this.sys.game.loop.time.toString())
 pointsText.setText('Points:' + points.toString())
// only if the player is alive
if (!this.isPlayerAlive) {
  return;
}
// check for active input
if (this.input.activePointer.isDown) {
  // player walks
  this.player.x += this.playerSpeed;
}
// treasure collision
if (Phaser.Geom.Intersects.RectangleToRectangle(this.player.getBounds(), this.treasure.getBounds())) {
  this.gamePrize();
  gameScene.liveGain();
 
}
// enemy movement and collision
let enemies = this.enemies.getChildren();
let numEnemies = enemies.length;
for (let i = 0; i < numEnemies; i++) {
  // move enemies
  enemies[i].y += enemies[i].speed;
  // reverse movement if reached the edges
  if (enemies[i].y >= this.enemyMaxY && enemies[i].speed > 0) {
    enemies[i].speed *= -1;
  } else if (enemies[i].y <= this.enemyMinY && enemies[i].speed < 0) {
    enemies[i].speed *= -1;
  }
  // enemy collision
  if (Phaser.Geom.Intersects.RectangleToRectangle(this.player.getBounds(), enemies[i].getBounds())) {
    //this.gameOver();
    gameScene.liveLoss();
    gameScene.pointsLose();
    if(lives == 0) {
      gameScene.gameOver();
      lives = 82;
      points = 0;
    }
    break;
  }
}
};
gameScene.gameOver = function() {
// flag to set player is dead
this.isPlayerAlive = false;
// shake the camera
this.cameras.main.shake(500);
// fade camera
this.time.delayedCall(250, function() {
  this.cameras.main.fade(250);
}, [], this);
// restart game
this.time.delayedCall(500, function() {
  this.scene.restart();
}, [], this);
};
// our game's configuration
let config = {
type: Phaser.AUTO,
width: 640,
height: 360,
scene: gameScene
};
// create the game, and pass it the configuration
let game = new Phaser.Game(config);
gameScene.gamePrize = function() {
   // flag to set player is dead
this.isPlayerAlive = true;
 gameScene.pointsEarn();
//Coin placing and scale
this.coin = this.add.sprite(320, 220, 'coin' )
//restart with delay
  this.time.delayedCall(500, function() {
    this.scene.restart();
  }, [], this);
};
